//
//  ViewController.m
//  BLEShare
//
//  Created by Mikhail Baynov on 09/01/15.
//  Copyright (c) 2015 Mikhail Baynov. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"





@interface ViewController ()
{
	AppDelegate *appDelegate;
	NSNotificationCenter *notificationCenter;
}
@end


@implementation ViewController


- (void)viewDidLoad {
	[super viewDidLoad];
	appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	notificationCenter = [NSNotificationCenter defaultCenter];
	
	[notificationCenter addObserver:self selector:@selector(gotData) name:@"gotData" object:nil];
	[notificationCenter addObserver:self selector:@selector(unsubscribed) name:@"unsubscribed" object:nil];
	//    _central = [Central new];
	

	
}



- (void)gotData
{
	self.imageView.image = appDelegate.imageRecieved;
	[appDelegate.bledevice.peripheralManager stopAdvertising];
}

- (void)unsubscribed
{
//	[self.advertisingSwitch setOn:NO animated:YES];
//	self.button.selected = NO;
	[appDelegate.bledevice.peripheralManager stopAdvertising];
//	self.textField.text = nil;
}




- (void)viewWillDisappear:(BOOL)animated
{
	//    [self.central.centralManager stopScan];
	[super viewWillDisappear:animated];
}


- (IBAction)shareButtonPressed:(UIButton *)sender {
	UIImage *pic = [UIImage imageNamed:@"gang1024.png"];
	
	appDelegate.dataToSend = UIImagePNGRepresentation(pic).mutableCopy;
	[appDelegate.bledevice.peripheralManager startAdvertising:
	 @{ CBAdvertisementDataServiceUUIDsKey : @[[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID]] }];

}



@end
