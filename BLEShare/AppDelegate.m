//
//  AppDelegate.m
//  BLEShare
//
//  Created by Mikhail Baynov on 09/01/15.
//  Copyright (c) 2015 Mikhail Baynov. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//	self.window = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
//	self.window.rootViewController = [ViewController new];
//	[self.window makeKeyAndVisible];
//	
	_central = [Central new];
	_bledevice = [BLEDevice new];
	_dataToSend = [NSMutableData new];
	_dataRecieved = [NSMutableData new];
	_imageRecieved = [UIImage new];
	
	return YES;
}






- (NSURL *)applicationDocumentsDirectory {
	return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void)showAlert:(NSString *)title message:(NSString *)message
{
	[[[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
}
@end
