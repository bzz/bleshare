//
//  ViewController.h
//  BLEShare
//
//  Created by Mikhail Baynov on 09/01/15.
//  Copyright (c) 2015 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ViewController : UIViewController


@property (weak, nonatomic) IBOutlet UIImageView *imageView;



@end

