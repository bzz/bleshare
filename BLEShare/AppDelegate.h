//
//  AppDelegate.h
//  BLEShare
//
//  Created by Mikhail Baynov on 09/01/15.
//  Copyright (c) 2015 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>

///USE Terminal mb$   uuidgen
#define TRANSFER_SERVICE_UUID           @"E20A39F4-73F5-4BC4-A12F-17D1AD07A961"
#define TRANSFER_CHARACTERISTIC_UUID    @"08590F7E-DB05-467E-8757-72F6FAEB13D4"
#define NOTIFY_MTU      20



#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "Central.h"
#import "BLEDevice.h"




@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) Central *central;
@property (strong, nonatomic) BLEDevice *bledevice;

@property (strong, nonatomic) NSMutableData *dataToSend;
@property (strong, nonatomic) NSMutableData *dataRecieved;

@property (strong, nonatomic) UIImage *imageRecieved;





- (NSURL *)applicationDocumentsDirectory;
- (void)showAlert:(NSString *)title message:(NSString *)message;


@end

