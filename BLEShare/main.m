//
//  main.m
//  BLEShare
//
//  Created by Mikhail Baynov on 09/01/15.
//  Copyright (c) 2015 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
